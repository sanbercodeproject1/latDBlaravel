<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pageRegister() {
        return view('Page.register');
    }
    public function pageWelcome(Request $request) {
        $firstNama = $request['first-name'];
        $lastNama = $request['last-name'];
        $alamat = $request['alamat'];

        return view('Page.welcome', ['firstNama' => $firstNama, 'lastNama' => $lastNama, 'alamat' => $alamat]);
    }
}
