<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create()
    {
        return view('Cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'biodata' => 'required'
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'biodata' => $request['biodata']
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('Cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('Cast.show', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('Cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'biodata' => 'required'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'biodata' => $request->biodata,
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }

}
