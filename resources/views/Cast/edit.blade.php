@extends('Layout.master')

@section('tittle')
  Edit Cast Page
@endsection

@section('card')
  CARD
@endsection

@section('content')

<div>
  <form action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control" name="title" value="{{$cast->nama}}" id="title" placeholder="Masukkan Title">
      </div>
      <div class="form-group">
        <label for="date">Umur</label>
        <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="date" placeholder="Masukkan Date">
    </div>
      <div class="form-group">
          <label for="body">Biodata</label>
          <input type="text" class="form-control" name="body"  value="{{$cast->biodata}}"  id="body" placeholder="Masukkan Body">
      </div>
      <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>

@endsection