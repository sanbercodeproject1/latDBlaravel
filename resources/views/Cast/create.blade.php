@extends('Layout.master')

@section('tittle')
  Cast Page
@endsection

@section('card')
  CARD
@endsection

@section('content')
<div>
  <h2>Tambah Data</h2>
      <form action="/cast" method="POST">
          @csrf
          <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
            <label for="title">Date</label>
            <input type="text" class="form-control" name="umur" id="date" placeholder="Masukkan Date">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
          <div class="form-group">
              <label for="body">body</label>
              <input type="text" class="form-control" name="biodata" id="body" placeholder="Masukkan Body">
              @error('biodata')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <button type="submit" class="btn btn-primary">Tambah</button>
      </form>
</div>
@endsection