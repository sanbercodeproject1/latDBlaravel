@extends('Layout.master')

@section('tittle')
  Data Cast Page
@endsection

@section('card')
  CARD
@endsection

@section('content')

<h2>Show Post {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<h5>{{$cast->umur}}</h5>
<p>{{$cast->biodata}}</p>

<a href="/cast" class="btn btn-info">Kembali</a>

@endsection