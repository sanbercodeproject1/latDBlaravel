@extends('Layout.master')

@section('tittle')
  Data Cast Page
@endsection

@section('card')
  CARD
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->biodata}}</td>
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Detail</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>

@endsection